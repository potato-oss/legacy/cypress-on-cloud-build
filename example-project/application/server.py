from http.server import HTTPServer, BaseHTTPRequestHandler


class ExampleServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write((
            "<html><head><title>Example</title></head>"
            "<body>Hello World!</body>"
            "</html>"
        ).encode("utf-8"))


server_address = ('', 8000)
httpd = HTTPServer(server_address, ExampleServer)
httpd.serve_forever()
