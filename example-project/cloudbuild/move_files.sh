#!/bin/bash

# In the Dockerfile, we installed the dependencies in the working directory /code/application/,
# so we now need to copy those into the Cloud Build WORKDIR (which is /workspace/) in order to have
# our installed files.

# This is currently unnecessary, as this example project doesn't install anything, but you might
# need this for your project.
# cp -rT /code/application/ /workspace/
